using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Momiviento : MonoBehaviour
{
    
    public Animator anim;
    public Rigidbody rb;

    //correr y salto
    public float movementSPeed, movementRotate,fuerzaDeSalto;
    public int limiteDeSaltos = 0;
    public bool dejaDecorrer, puedeSaltar = false;

    //camara
    //public Camera camara;
    //public Vector3 offset;
    //public Transform target;
    //[Range(0, 1)] public float lerpValue;
    //public float sensibilidad;

    private void Start()
    {
      //target= GameObject.Find("Player").transform;
    }
    private void LateUpdate()
    {
        //camara.transform.position = Vector3.Lerp(transform.position, target.position + offset, lerpValue);
        //offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X")*sensibilidad,Vector3.up)*offset;
        //transform.LookAt(target);
    }
    void Update()
    {
        Vector3 moventDrection = new Vector3(0, 0, Input.GetAxis("Vertical"));
        anim.SetFloat("velocidad", Mathf.Abs(moventDrection.z));
        transform.Translate((moventDrection * movementSPeed) * Time.deltaTime);
        transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal")*movementRotate, 0));

        if (Input.GetButtonDown("Fire2"))
        {
            anim.SetBool("Jump", true);
            StartCoroutine("correr");
            dejaDecorrer = true;
        }
        if (!Input.GetButton("Fire2") && dejaDecorrer == true)
        {
            anim.SetBool("Jump", false);
            StartCoroutine("Nocorrer");
            dejaDecorrer = false;
        }
        if (Input.GetButton("Jump")&&limiteDeSaltos<=0)
        {
            StartCoroutine("salto");
        }
        if (!Input.GetButton("Jump") && puedeSaltar == true)
        {

            anim.SetBool("salto", false);
            puedeSaltar= false;
        }
       
    }
    IEnumerator correr()
    {
        movementSPeed = movementSPeed + 3;
        yield return new WaitForSeconds(2);
        movementSPeed = movementSPeed + 7;
    }
    IEnumerator Nocorrer()
    {
        movementSPeed = movementSPeed - 3;
        yield return new WaitForSeconds(2);
        movementSPeed = movementSPeed - 7;
    }

    IEnumerator salto()
    {
        limiteDeSaltos = 1;
        anim.SetBool("salto", true);
        puedeSaltar = true;

        if (puedeSaltar == true)
        {
            rb.AddForce(new Vector3(0, fuerzaDeSalto, 0), ForceMode.Impulse);
            yield return new WaitForSeconds(2);
            limiteDeSaltos = 0;
        }
    }
   
}

