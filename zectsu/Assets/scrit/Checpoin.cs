using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checpoin : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            collision.GetComponent<PayerRespaw>().ReachedCheckpoin(transform.position.x,transform.position.y);
        }
    }

}
