using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PayerRespaw : MonoBehaviour
{
    private float checkPointPositionX, checkPointPositionY;
    void Start()
    {
       if(PlayerPrefs.GetFloat("checkPointPositionX")!=0)
       {
            transform.position = (new Vector3(PlayerPrefs.GetFloat("checkPointPositionX"), PlayerPrefs.GetFloat("checkPointPositionY")));
       }
    }
    void Update()
    {
        
    }

    public void ReachedCheckpoin(float x, float y)
    {
        PlayerPrefs.SetFloat("checkPointPositionX",x);
        PlayerPrefs.SetFloat("checkPointPositionY",y);
    }
}
